package com.example.helloandroid;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends Activity {

    public final static String EXTRA_MESSAGE = "com.example.helloandroid.MESSAGE";
    public final static String EXTRA_RADIO = "com.example.helloandroid.RADIO";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    	EditText editText = (EditText) findViewById(R.id.edit_message);
    	editText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {

				if(s.toString().contains("1")) {
					sendToast("I don't like 1");
					s.clear();
				}
				
			}
		});
    }
    
    private void sendToast(String message)
    {

		Context context = this;
		CharSequence text = message;
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
    }

	public void sendMessage(View v)
    {
    	Intent intent = new Intent(this, DisplayMessageActivity.class);
    	EditText editText = (EditText) findViewById(R.id.edit_message);
    	RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radio_group);
    	String message = editText.getText().toString();
    	if(message.toString().isEmpty())
    	{
    		sendToast("Please enter text!");
    	} else {
	    	intent.putExtra(EXTRA_MESSAGE, message);
	    	intent.putExtra(EXTRA_RADIO, radioGroup.getCheckedRadioButtonId());
	        startActivity(intent);
    	}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
